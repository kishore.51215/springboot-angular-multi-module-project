package sample.multimodule.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@ToString
@ApiModel(value = "finances.EmployeeDTO", description = "DTO for the Employee entity")
public class EmployeeDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String emailId;
}
