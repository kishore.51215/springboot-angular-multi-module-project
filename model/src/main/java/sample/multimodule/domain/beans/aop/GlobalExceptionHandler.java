package sample.multimodule.domain.beans.aop;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import sample.multimodule.domain.beans.error.ErrorDetails;
import sample.multimodule.domain.exception.ResourceNotFoundException;

import java.util.Date;

/**
 * This class is used to handle exception specific
 * and global exception in a single place.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex,
                                                       WebRequest request) {

        ErrorDetails errorDetails = new ErrorDetails(new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globlaExceptionHandler(Exception ex,
                                                       WebRequest request) {

        ErrorDetails errorDetails = new ErrorDetails(new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
