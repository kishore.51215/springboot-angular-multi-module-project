package sample.multimodule.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import sample.multimodule.domain.entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

//    ResponseEntity<Employee> getEmployeeById(); // throws ResourceNotFoundException;
}
