package sample.multimodule.web.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sample.multimodule.domain.dto.EmployeeDTO;
import sample.multimodule.service.api.EmployeeService;
import sample.multimodule.domain.exception.ResourceNotFoundException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * To enable CORS on the server, the <b>@CrossOrigin</b> annotation
 * should be added.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@RestController
@Api(value = "EmployeeApi")
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class EmployeeController {

//    @Autowired
    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @ApiOperation(value = "Retrieves employees",
    httpMethod = "GET",
    consumes = "application/json",
    produces = "application/json",
    response = List.class)
    @GetMapping("/employees")
    public List<EmployeeDTO> getAllEmployees() {
        return employeeService.getAll();
    }

    @ApiOperation(value = "Retrieve the employee with that id",
            httpMethod = "GET",
            consumes = "application/json",
            produces = "application/json",
            response = EmployeeDTO.class)
    @GetMapping("/employees/{id}")
    public EmployeeDTO getEmployeeById(@PathVariable(value = "id") Long employeeId)
            throws ResourceNotFoundException {
        return employeeService.getEmployeeById(employeeId);
    }

    @ApiOperation(value = "To create a new employee",
            httpMethod = "POST",
            consumes = "application/json",
            produces = "application/json",
            response = EmployeeDTO.class)
    @PostMapping("/employees")
    public EmployeeDTO createEmployee(@Valid @RequestBody EmployeeDTO employee) {
        return employeeService.createEmployee(employee);
    }

    @ApiOperation(value = "To update an employee",
            httpMethod = "PUT",
            consumes = "application/json",
            produces = "application/json",
            response = EmployeeDTO.class)
    @PostMapping("/employees/{id}")
    public EmployeeDTO updateEmployee(@PathVariable(value = "id") Long employeeId,
                                      @Valid @RequestBody EmployeeDTO employeeDetails)
            throws ResourceNotFoundException{

        return employeeService.updateEmployee(employeeId, employeeDetails);
    }

    @ApiOperation(value = "To delete an employee",
            httpMethod = "DELETE",
            consumes = "application/json",
            produces = "application/json",
            response = Map.class)
    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
            throws ResourceNotFoundException {

        return employeeService.deleteEmployee(employeeId);
    }

}
